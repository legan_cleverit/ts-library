// Made with 💜 by
// ______     __         ______     __   __   ______     ______     __     ______
// /\  ___\   /\ \       /\  ___\   /\ \ / /  /\  ___\   /\  == \   /\ \   /\__  _\
// \ \ \____  \ \ \____  \ \  __\   \ \ \'/   \ \  __\   \ \  __<   \ \ \  \/_/\ \/
//  \ \_____\  \ \_____\  \ \_____\  \ \__|    \ \_____\  \ \_\ \_\  \ \_\    \ \_\
//   \/_____/   \/_____/   \/_____/   \/_/      \/_____/   \/_/ /_/   \/_/     \/_/
//

// * This file is a compilation of utility functions for managing files
// * for BACK-END development
// * for everyone to use in times of need.

// * Made for clevers by clevers.

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// *****************************************
// * Generate text file
// *****************************************
interface IGenTextFile {
  text: string;
  outDir: string;
}
const genTextFile = ({ text, outDir }: IGenTextFile): void => {
  const fs = require("fs");
  fs.writeFileSync(`${outDir}.txt`, text);
};
