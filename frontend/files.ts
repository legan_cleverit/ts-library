// Made with 💜 by
// ______     __         ______     __   __   ______     ______     __     ______  
// /\  ___\   /\ \       /\  ___\   /\ \ / /  /\  ___\   /\  == \   /\ \   /\__  _\ 
// \ \ \____  \ \ \____  \ \  __\   \ \ \'/   \ \  __\   \ \  __<   \ \ \  \/_/\ \/ 
//  \ \_____\  \ \_____\  \ \_____\  \ \__|    \ \_____\  \ \_\ \_\  \ \_\    \ \_\ 
//   \/_____/   \/_____/   \/_____/   \/_/      \/_____/   \/_/ /_/   \/_/     \/_/ 
//                                                                                  


// * This file is a compilation of utility functions for managing files
// * for FRONT-END development
// * for everyone to use in times of need.

// * Made for clevers by clevers.

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// *****************************************
// * Get Base64 string from Blob object
// *****************************************
interface IBlobToBase {
  blob: Blob;
}
const blobToBase64 = ({ blob }: IBlobToBase) => {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.onload = async () => {
      resolve((reader.result as string).replace(/^data:.+;base64,/, ""));
    };
    reader.readAsDataURL(blob);
  });
};

// *****************************************
// * Trigger download from blob or url
// *****************************************
interface IDownloadFile {
  blob?: Blob;
  fileUrl?: string;
  fileName: string;
}
const downloadFile = ({ blob, fileUrl, fileName }: IDownloadFile) => {
  const href = blob ? window.URL.createObjectURL(blob) : fileUrl;

  const anchor = document.createElement("a");
  anchor.setAttribute("href", href);
  anchor.setAttribute("download", fileName);
  document.body.appendChild(anchor);
  anchor.click();
  anchor.parentNode.removeChild(anchor);
};
